<h1> ZEUSZ 2021 desktop </h1>

[programozas.org](https://programozas.org/)

Magyarországon 2000 óta kiemeltadós termék a bor. Úgynevezett jövedéki termék kategóriába került. Azóta már nem tartalmaz jövedéki adót de a termelésének az ellenörzése továbbra is megmaradt. Cégünk továbbá sör és szesz adóraktárak könyvelési rendszerének fejlesztésével is foglalkozik. Valamint ezen adóraktárak nagykereskedelmi rendzserének fejlesztésével is.

A Zeusz jövedéki rendszer képes:
1. A törvény által szabályozott kimutatások előállítására
2. Úgynevezett EKO, TKO, BKO bizonylatok kiállítására
3. Online számla előállítására és beküldésére

<img src="desktop.jpg" alt="Desktop Zeusz">

1. A fejlesztész naprakész leírása [ezen a linken](https://gitlab.com/programozas.kft/zeusz-desktop/-/blob/main/zeusz.pdf)) olvasható !
2. A programról készült videók [ezen a linken](https://www.youtube.com/c/K%C3%A9v%C3%A9sJ%C3%A1nos/videos) nézhető !
3. A programról készült képek [ezen a linken](https://photos.google.com/share/AF1QipMIvi3FjOys2QVZYiLNdNjj1TqZuN1usQITM0roFJ4CY_4Umh9YvPPCBKT3RrOf0A?key=WUp6NF9VdGhycVhfVWVqdDFZYVgydE8wamxiT213) nézhető !

